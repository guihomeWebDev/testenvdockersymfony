<?php

namespace App\Repository;

use App\Entity\ScoringBoard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ScoringBoard|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScoringBoard|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScoringBoard[]    findAll()
 * @method ScoringBoard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScoringBoardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScoringBoard::class);
    }

    // /**
    //  * @return ScoringBoard[] Returns an array of ScoringBoard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScoringBoard
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
